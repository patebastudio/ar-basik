using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class ARManagement : MonoBehaviour
{
    [Header("Button Target Found And Lost")]
    public GameObject CubeAnimationButton;
    public GameObject BlockAnimationButton;
    public GameObject PrismAnimationButton;
    public GameObject PyramidAnimationButton;
    public GameObject SphereAnimationButton;
    public GameObject ConeAnimationButton;
    public GameObject CylinderAnimationButton;

    [Header("Cube Nets Button")]
    public GameObject CubeNetsOpen;
    public GameObject CubeNetsClose;

    [Header("Block Nets Button")]
    public GameObject BlockNetsOpen;
    public GameObject BlockNetsClose;

    [Header("Prism Nets Button")]
    public GameObject PrismNetsOpen;
    public GameObject PrismNetsClose;

    [Header("Pyramid Nets Button")]
    public GameObject PyramidNetsOpen;
    public GameObject PyramidNetsClose;

    [Header("Sphere Nets Button")]
    public GameObject SphereNetsOpen;
    public GameObject SphereNetsClose;

    [Header("Cone Nets Button")]
    public GameObject ConeNetsOpen;
    public GameObject ConeNetsClose;

    [Header("Cylinder Nets Button")]
    public GameObject CylinderNetsOpen;
    public GameObject CylinderNetsClose;

    // Start is called before the first frame update
    void Start()
    {
        CubeAnimationButton.SetActive(false);
        BlockAnimationButton.SetActive(false);
        PrismAnimationButton.SetActive(false);
        PyramidAnimationButton.SetActive(false);
        SphereAnimationButton.SetActive(false);
        ConeAnimationButton.SetActive(false);
        CylinderAnimationButton.SetActive(false);

        CubeNetsOpen.SetActive(true);
        BlockNetsOpen.SetActive(true);
        PrismNetsOpen.SetActive(true);
        PyramidNetsOpen.SetActive(true);
        SphereNetsOpen.SetActive(true);
        ConeNetsOpen.SetActive(true);
        CylinderNetsOpen.SetActive(true);

        CubeNetsClose.SetActive(false);
        BlockNetsClose.SetActive(false);
        PrismNetsClose.SetActive(false);
        PyramidNetsClose.SetActive(false);
        SphereNetsClose.SetActive(false);
        ConeNetsClose.SetActive(false);
        CylinderNetsClose.SetActive(false);
    }

    public void ChangeSceneButton(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }

    public void CubeTargetFound()
    {
        CubeAnimationButton.SetActive(true);
    }
    public void CubeTargetLost()
    {
        CubeAnimationButton.SetActive(false);
    }

    public void BlockTargetFound()
    {
        BlockAnimationButton.SetActive(true);
    }
    public void BlockTargetLost()
    {
        BlockAnimationButton.SetActive(false);
    }

    public void PrismTargetFound()
    {
        PrismAnimationButton.SetActive(true);
    }
    public void PrismTargetLost()
    {
        PrismAnimationButton.SetActive(false);
    }

    public void PyramidTargetFound()
    {
        PyramidAnimationButton.SetActive(true);
    }
    public void PyramidTargetLost()
    {
        PyramidAnimationButton.SetActive(false);
    }

    public void SphereTargetFound()
    {
        SphereAnimationButton.SetActive(true);
    }
    public void SphereTargetLost()
    {
        SphereAnimationButton.SetActive(false);
    }

    public void ConeTargetFound()
    {
        ConeAnimationButton.SetActive(true);
    }
    public void ConeTargetLost()
    {
        ConeAnimationButton.SetActive(false);
    }

    public void CylinderTargetFound()
    {
        CylinderAnimationButton.SetActive(true);
    }
    public void CylinderTargetLost()
    {
        CylinderAnimationButton.SetActive(false);
    }

    public void CubeNetsOpenButton()
    {
        CubeNetsOpen.SetActive(false);
        CubeNetsClose.SetActive(true);
    }
    public void CubeNetsCloseButton()
    {
        CubeNetsOpen.SetActive(true);
        CubeNetsClose.SetActive(false);
    }

    public void BlockNetsOpenButton()
    {
        BlockNetsOpen.SetActive(false);
        BlockNetsClose.SetActive(true);
    }
    public void BlockNetsCloseButton()
    {
        BlockNetsOpen.SetActive(true);
        BlockNetsClose.SetActive(false);
    }

    public void PrismNetsOpenButton()
    {
        PrismNetsOpen.SetActive(false);
        PrismNetsClose.SetActive(true);
    }
    public void PrismNetsCloseButton()
    {
        PrismNetsOpen.SetActive(true);
        PrismNetsClose.SetActive(false);
    }

    public void PyramidNetsOpenButton()
    {
        PyramidNetsOpen.SetActive(false);
        PyramidNetsClose.SetActive(true);
    }
    public void PyramidNetsCloseButton()
    {
        PyramidNetsOpen.SetActive(true);
        PyramidNetsClose.SetActive(false);
    }

    public void SphereNetsOpenButton()
    {
        SphereNetsOpen.SetActive(false);
        SphereNetsClose.SetActive(true);
    }
    public void SphereNetsCloseButton()
    {
        SphereNetsOpen.SetActive(true);
        SphereNetsClose.SetActive(false);
    }

    public void CylinderNetsOpenButton()
    {
        CylinderNetsOpen.SetActive(false);
        CylinderNetsClose.SetActive(true);
    }
    public void CylinderNetsCloseButton()
    {
        CylinderNetsOpen.SetActive(true);
        CylinderNetsClose.SetActive(false);
    }

    public void ConeNetsOpenButton()
    {
        ConeNetsOpen.SetActive(false);
        ConeNetsClose.SetActive(true);
    }
    public void ConeNetsCloseButton()
    {
        ConeNetsOpen.SetActive(true);
        ConeNetsClose.SetActive(false);
    }
}
