using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Runtime.InteropServices;
using UnityEngine.Video;
using System;
using TMPro;

[Serializable]
public class DYKDetail
{
    public GameObject DYKPanel;
    public VideoClip DYKVideoClip;
    public string DYKVideoSource;
}

public class MenuManagement : MonoBehaviour
{
    [Header("Main Panel")]
    public GameObject HomePanel;
    public GameObject DoYouKnowPanel;
    public GameObject KDPanel;
    public GameObject GuidePanel;

    [Header("Do You Know")]
    public int DYKIndex;
    public float skipVideoFactor;
    public VideoPlayer videoPlayer;
    public TextMeshProUGUI videoSourceText;
    public AudioSource bgmSource;
    public GameObject playButton;
    public GameObject pauseButton;
    public GameObject prevButton;
    public GameObject nextButton;
    public List<DYKDetail> DYKDetails;

    // Start is called before the first frame update
    void Start()
    {
        HomePanel.SetActive(true);
        DoYouKnowPanel.SetActive(false);
        KDPanel.SetActive(false);
        GuidePanel.SetActive(false);
    }

    public void ChangeSceneButton(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }

    public void DoYouKnowButton()
    {
        HomePanel.SetActive(false);
        DoYouKnowPanel.SetActive(true);
        KDPanel.SetActive(false);
        GuidePanel.SetActive(false);

        DYKIndex = 0;
        SetDYKPanelState();
    }

    public void PlayVideo()
    {
        playButton.SetActive(false);
        pauseButton.SetActive(true);

        videoPlayer.clip = DYKDetails[DYKIndex].DYKVideoClip;
        videoPlayer.Play();
        if (bgmSource.isPlaying)
        {
            bgmSource.Pause();
        }
    }

    public void PauseVideo()
    {
        playButton.SetActive(true);
        pauseButton.SetActive(false);

        videoPlayer.Pause();
        if (!bgmSource.isPlaying)
        {
            bgmSource.Play();
        }
    }

    public void StopVideo()
    {
        playButton.SetActive(true);
        pauseButton.SetActive(false);

        videoPlayer.Stop();
        if (!bgmSource.isPlaying)
        {
            bgmSource.Play();
        }
    }

    public void SkipVideo(int factor)
    {
        videoPlayer.time += (skipVideoFactor * factor);
    }

    public void NextDYKPanel()
    {
        StopVideo();
        DYKIndex++;
        SetDYKPanelState();
    }

    public void PrevDYKPanel()
    {
        StopVideo();
        DYKIndex--;
        SetDYKPanelState();
    }

    public void SetDYKPanelState()
    {
        if (!bgmSource.isPlaying)
        {
            bgmSource.Play();
        }

        foreach (DYKDetail dyk in DYKDetails)
        {
            dyk.DYKPanel.SetActive(false);
        }

        playButton.SetActive(true);
        pauseButton.SetActive(false);
        prevButton.SetActive(true);
        nextButton.SetActive(true);
        DYKDetails[DYKIndex].DYKPanel.SetActive(true);
        videoSourceText.text = $"Sumber: {DYKDetails[DYKIndex].DYKVideoSource}";

        if (DYKIndex == 0) prevButton.SetActive(false);
        else if (DYKIndex == DYKDetails.Count - 1) nextButton.SetActive(false);
    }

    public void KDButton()
    {
        HomePanel.SetActive(false);
        DoYouKnowPanel.SetActive(false);
        KDPanel.SetActive(true);
        GuidePanel.SetActive(false);
    }

    public void GuideButton()
    {
        HomePanel.SetActive(false);
        DoYouKnowPanel.SetActive(false);
        KDPanel.SetActive(false);
        GuidePanel.SetActive(true);
    }

    public void BackButton()
    {
        HomePanel.SetActive(true);
        DoYouKnowPanel.SetActive(false);
        KDPanel.SetActive(false);
        GuidePanel.SetActive(false);
    }

    public void QuitApplicationButton()
    {
        Application.Quit();
    }
}
