﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class QuizManagement : MonoBehaviour
{
    [Header("Panel")]
    public GameObject quizpanel;

    [Header("Button")]
    public GameObject restartbutton;

    [System.Serializable]
    public class Question
    {
        //Tempat mengisi pertanyaan
        [TextArea]
        [Header("Question")]
        public string question;

        //Tempat mengisi pilihan
        [Header("Option")]
        public string answerA;
        public string answerB;
        public string answerC;
        public string answerD;

        //Kunci jawaban benar
        [Header("Answer Key")]
        public bool A;
        public bool B;
        public bool C;
        public bool D;
    }
#pragma warning disable

    public int score;
    public float time;
    private int randomvalues;
    Text questiontext,optionA,optionB,optionC,optionD,detailstamp;
    public List<Question> questionrange;

    // Mencari teks soal dan opsi pilihan A,B,C,D
    void Start()
    {
        quizpanel.SetActive(true);

        questiontext = GameObject.Find("Question Text").GetComponent<Text>();
        optionA = GameObject.Find("AnswerA Text").GetComponent<Text>();
        optionB = GameObject.Find("AnswerB Text").GetComponent<Text>();
        optionC = GameObject.Find("AnswerC Text").GetComponent<Text>();
        optionD = GameObject.Find("AnswerD Text").GetComponent<Text>();
        detailstamp = GameObject.Find("Detail Text").GetComponent<Text>();
        randomvalues = Random.RandomRange(0, questionrange.Count);

        restartbutton.SetActive(false);
    }

    // Berjalan secara realtime atau terus menerus jika ada yang berubah
    void Update()
    {
        detailstamp.text = "Waktu : " + time.ToString("0") + "\nSkor : " + score;
        time -= Time.deltaTime;
        if (time <= 0)
        {
            questionrange.RemoveAt(randomvalues);
            //Waktu awal 30 detik
            time = 45;
            //Mengacak soal jika waktu telah mencapai 0
            randomvalues = Random.RandomRange(0, questionrange.Count);
        }
        if (questionrange.Count > 0)
        {
            quizpanel.GetComponent<VerticalLayoutGroup>().enabled = false;
            quizpanel.GetComponent<VerticalLayoutGroup>().enabled = true;
            questiontext.text = questionrange[randomvalues].question;
            optionA.text = questionrange[randomvalues].answerA;
            optionB.text = questionrange[randomvalues].answerB;
            optionC.text = questionrange[randomvalues].answerC;
            optionD.text = questionrange[randomvalues].answerD;
        }
        else {
            //Menampilkan skor akhir yang didapat setelah menjawab keseluruhan soal
            questiontext.text = "score Yang Telah Kamu Peroleh Adalah :" + score;
            GameObject.Find("OptionA Button").SetActive(false);
            GameObject.Find("OptionB Button").SetActive(false);
            GameObject.Find("OptionC Button").SetActive(false);
            GameObject.Find("OptionD Button").SetActive(false);
            restartbutton.SetActive(true);
        }
    }

    //Memeriksa jawaban benar,jika benar akan bertambah 1 point
    public void AnswerCheck(string answer)
    {
        if (questionrange[randomvalues].A == true && answer == "a")
        {
            score++;
        }
        if (questionrange[randomvalues].B == true && answer == "b")
        {
            score++;
        }
        if (questionrange[randomvalues].C == true && answer == "c")
        {
            score++;
        }
        if (questionrange[randomvalues].D == true && answer == "d")
        {
            score++;
        }

        //Mengambil ulang soal acak setelah menjawab
        questionrange.RemoveAt(randomvalues);
        randomvalues = Random.RandomRange(0, questionrange.Count);
        //Reset waktu ke 30 detik
        time = 45;
    }

    //Ulangi Kuis
    public void RestartGame()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

    public void ChangeScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }
#pragma warning restore
}
